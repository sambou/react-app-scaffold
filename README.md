## React App Scaffold
My current scaffold for TypeScript apps with React & Redux. Use at your own risk. PRs are more than welcome.

[![Dependency Status](https://david-dm.org/sambou/react-app-scaffold.svg)](https://david-dm.org/sambou/react-app-scaffold)
[![devDependency Status](https://david-dm.org/sambou/react-app-scaffold/dev-status.svg)](https://david-dm.org/sambou/react-app-scaffold#info=devDependencies)
[![Build Status](https://travis-ci.org/sambou/react-app-scaffold.svg?branch=master)](https://travis-ci.org/sambou/react-app-scaffold)
[![Demo on Heroku](https://img.shields.io/badge/heroku-demo-blue.svg)](https://react-app-scaffold.herokuapp.com)

## Setup
- ```npm install```
- ```npm start``` for dev server at localhost:3000
- ```npm test``` for unit tests
- ```npm run build``` gets run automatically in production environments via postinstall hook

## TODOs
- [ ] Add immutable data structures
- [ ] Add normalizr

## Misc
- Unit tests are run as a precommit hook
